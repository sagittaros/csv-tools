package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
)

var (
	CSV        string
	Output     string
	skipHeader bool
)

const (
	SolrXML = "solrxml"
	PG      = "pg"
	Ruby    = "ruby"
)

func init() {
	flag.StringVar(&CSV, "csv", "", "csv path")
	flag.BoolVar(&skipHeader, "skip-header", true, "skip header in CSV")
	flag.StringVar(&Output, "output", "solr", "type of output to expect")
	flag.Parse()
}

func main() {
	lines := loadListFromCsv(CSV)

	switch Output {
	case SolrXML:
		fmt.Println(SolrDeleteXML(lines))
	case PG:
		fmt.Println(PGConditionArray(lines))
	case Ruby:
		fmt.Println(RubyArray(lines))
	default:
		fmt.Println("Unsupported output")
	}
}

func loadListFromCsv(path string) []string {
	var lines []string
	csvFile, _ := os.Open(path)
	reader := csv.NewReader(bufio.NewReader(csvFile))

	if skipHeader {
		reader.Read() // omit header row
	}

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
		lines = append(lines, line[0])
	}
	return lines
}
