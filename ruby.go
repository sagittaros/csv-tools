package main

import (
	"fmt"
	"strings"
)

func RubyArray(list []string) string {
	var ids []string
	for _, line := range list {
		if len(line) > 0 {
			ids = append(ids, fmt.Sprintf("'%s'", line))
		}
	}

	return "[" + strings.Join(ids, ",") + "]"
}
