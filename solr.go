package main

import "fmt"

//  curl 'http://SOLR/update?commit=true' -H "Content-Type: text/xml" --data-binary '<delete><query>court:SGDT || court:SGAB</query></delete>'
func SolrDeleteXML(ids []string) string {
	concatenatedIDs := ""
	for _, id := range ids {
		concatenatedIDs += fmt.Sprintf("<id>%s</id>", id)
	}
	return fmt.Sprintf("<delete>%s</delete>", concatenatedIDs)
}
